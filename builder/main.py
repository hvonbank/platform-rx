from os.path import join
from SCons.Script import AlwaysBuild, Builder, Default, DefaultEnvironment

env = DefaultEnvironment()


# A full list with the available variables
# http://www.scons.org/doc/production/HTML/scons-user.html#app-variables
env.Replace(
    AR="rx-elf-ar",
    AS="rx-elf-as",
    CC="rx-elf-gcc",
    CXX="rx-elf-g++",
    OBJCOPY="rx-elf-objcopy",
    RANLIB="rx-elf-ranlib",

    UPLOADER="rxs",
    UPLOADERFLAGS=[
        "-p",
        "/dev/ttyUSB0"
    ],
    UPLOADCMD='$UPLOADER $UPLOADERFLAGS $SOURCES',
)

env.Append(
	BUILDERS=dict(
        ElfToBin=Builder(
            action=" ".join([
                "$OBJCOPY",
                "-O",
                "binary",
                "$SOURCES",
                "$TARGET"]),
            suffix=".bin"
        )
    )
)


#print env.Dump()

# The source code of "platformio-build-tool" is here
# https://github.com/platformio/platformio-core/blob/develop/platformio/builder/tools/platformio.py

#
# Target: Build executable and linkable firmware
#
target_elf = env.BuildProgram()

#
# Target: Build the .bin file
#
target_bin = env.ElfToBin(join("$BUILD_DIR", "firmware"), target_elf)

#
# Target: Upload firmware
#
upload = env.Alias(["upload"], target_bin, "$UPLOADCMD")
AlwaysBuild(upload)

#
# Target: Define targets
#
Default(target_bin)
